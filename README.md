# Majestic Trucks
<p><b>UBER like service for freight trucks</b></p>

## Usage

- You should have installed [NodeJS](https://nodejs.org/en/) 
- Fork and clone this repository
- Run `npm i` to install dependencies
- Run `npm start` to start the server and stop with Ctrl+C
- Go to [localhost](http://localhost:8080)
- After `/api/` select methods or use [POSTMAN](https://www.postman.com/) . Projects is still developing so
  it'd be easier in the future.

## Contributor

|                    Contributor                     |                Telegram                 |                               Gmail                               |
| :------------------------------------------------: | :-------------------------------------: | :---------------------------------------------------------------: |
| [Maksym Rozhytskyi](https://gitlab.com/Maksym1910) | [maksym_1910](https://t.me/maksym_1910) | [maksym.rozhytskyi@gmail.com](mailto:maksym.rozhytskyi@gmail.com) |

## Licence

[MIT Ⓒ Maksym Rozhytskyi](https://gitlab.com/Maksym1910/nodejs-majestic-trucks-service/-/blob/master/LICENSE)
